1. Crear todas las clases que estan en el diagrama.
2. Completar todas las propiedades y los metodos.
3. Agregar contadores para las "ACCOUNT" y todas sus herencias asi al momento de crear cualquier tarjeta se pueda saber
   cuentas tarjetas se han creado de cada objeto y tambien poder saber el total de cada clase padre (se tiene que
   modificar las clases y el diagrama en UML).
4. Agregar en la clase de account la opcion de poder buscar el numero de tarjeta y que este me devuelva el ID del
   cliente que tiene asignada este numero de tarjeta (Modificar clases y el diagrama en UML).